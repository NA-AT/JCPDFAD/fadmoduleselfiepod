//
//  Checkpoint.swift
//  FADModulePhoto
//
//  Created by Juan Carlos Pérez on 1/23/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit

class Checkpoint: NSObject {
    var idTypeProcess = -1
    var imgIdFront : Data?
    var imgIdBack : Data?
    var name = ""
    var fName = ""
    var mName = ""
    var curp = ""
    var year = 0
    var cic = ""
    var ocr = ""
    var email = ""
    var telephone = ""
    var address = ""
    var imgSelfie : Data?
    var percentage = ""
    var idRequisition = ""
    var idSigner = ""
    
    var key = ""
    var vector = ""
    var idHasAddress = false
    
    
    
}
