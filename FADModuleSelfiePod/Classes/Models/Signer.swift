//
//  Signer.swift
//  FADModulePhoto
//
//  Created by Juan Carlos Pérez on 1/23/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit

class Signer: NSObject {

    var idSigner: String = ""
    var idRequisition: String = ""
    var signerName: String = ""
    var authenticationType: String = ""
    var authenticationData: String = ""
    var typeOfSigner: Double = 0
    var status: Bool = false
    var dateSign = ""
    var latitude = ""
    var longitude = ""
    var statusCompleted = 0
    var M1: Data?
    
    var mail : String = ""
    var orderOfSigner : Int = 0
    var phone : String = ""
    var signed : Bool = false
    
    var deviceUuid : String = ""
    var deviceName : String  = ""
    var deviceModel : String = ""
    var operatingSystem : String = ""
    var operatingSystemVersion : String = ""
    var operatingSystemCompilation : String = ""
    var versionApp : String = ""
    
    var mailCompany : String = ""
    var signLog: Data?
}
