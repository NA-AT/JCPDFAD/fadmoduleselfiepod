//
//  PopupChooseCameraViewController.swift
//  FADModuleSelfiePod
//
//  Created by Juan Carlos Pérez on 3/2/19.
//

import UIKit

@objc protocol PopupChooseCameraViewControllerDelegate{
    func doneShowPopup(frontCamera : Bool)
}

class PopupChooseCameraViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var dismissView: UIView!
    
    var delegate : PopupChooseCameraViewControllerDelegate! = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.dissmissViewAction))
        self.dismissView.addGestureRecognizer(gesture)

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setStyle()
    
    }
    
    
    func showInView(_ aView: UIViewController!)
    {
        self.view.tag = 20
        aView.view.addSubview(self.view)
        self.showAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        },completion:{(finished : Bool)  in
            if (finished){
                //self.fatherView.navigationController?.setNavigationBarHidden(true, animated: true)
            }
        });
    }
    
    func removeAnimate()
    {
        //self.fatherView.navigationController?.setNavigationBarHidden(false, animated: false)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished){
                //
                self.view.removeFromSuperview()
            }
        });
    }
    
    
    
    internal func setStyle(){
        //Estilo boton Escolar
        //self.btnAdd.backgroundColor = UIColor.init(hex: SAColor00226C).withAlphaComponent(0.3)
        self.contentView.layer.cornerRadius = 25
        self.contentView.layer.borderWidth = 3
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        
    }

 
    @IBAction func selectFrontCamera(_ sender : UIButton){
        self.removeAnimate()
        delegate?.doneShowPopup(frontCamera: true)
    }
    
    @IBAction func selectBackCamera(_ sender : UIButton){
        self.removeAnimate()
        delegate?.doneShowPopup(frontCamera: false)
    }
    
    
    @objc func dissmissViewAction(){
        self.removeAnimate()
    }

}
