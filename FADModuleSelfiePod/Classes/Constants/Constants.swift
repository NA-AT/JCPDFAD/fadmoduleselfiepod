//
//  Constants.swift
//  FAD
//
//  Created by Eduardo Martinez Calderòn on 30/05/17.
//  Copyright © 2017 NA-AT. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    ///IP`s
    static var FADIP    = "https://api.firmaautografa.com/"//https://api.firmaautografadigital.com
    static var FADDEVIP = "http://148.223.241.102:7001/api/api/"
    
    // "https://develop.firmaautografadigital.com"
    // "https://api.firmaautografadigital.com"
    // PROD "http://ec2-18-220-3-144.us-east-2.compute.amazonaws.com:7009"
    // DEV "http://ec2-54-219-170-36.us-west-1.compute.amazonaws.com:7009"
    // TEST "http://23.253.243.17:8183"
    // PRDO "http://23.253.243.17:8088"
    // ventas http://104.130.241.72
    static var ICONSIP = "https://demo.firmaautografa.com"
    // TEST  "http://23.253.243.17:8184"
    // PROD "http://23.253.243.17:8089"
    // ventas = "http://104.130.241.72"
    static var SERVICESURL = "\(FADIP)/fad/services/api/contract"
    static var CLIENTSERVICE = ICONSIP + "/services/api/client/consultClients"
    static let CLIENTICON = ICONSIP + "/services/api/client/getIcon/"
    static var timeout_request = 60
    
    /// Token
    static var TOKENURL = FADIP + "/authorization-server/oauth/token"//"http://23.253.243.17/auth/oauth/token"
    static var REFRESHTOKENURL = FADIP + "/authorization-server/oauth/token"//"http://23.253.243.17/auth/oauth/token"
    static var TESTURL = "http://192.168.1.178:9090/message"
    static var isEnableOAUTH = false
   
    /// Splash
    static var EXPIRATIONHOURS = 720
    static var AVAILABLEFREESPACE = 300.0
    static var AVAILABLEFREERAM :UInt32 = UInt32(50.0)
    static let ISICONDYNAMIC = false
    
    /// Appdelegate
    static let URLSCHEME = "fadticket"
    
    /// OCR
    static let isOCR = false
    static let isONYX = true
    
    //FADCUBAMEX
    static let isCUBAMEX = true
    static let isCUBAMEXiPad = false
    
    static var isSOCracked = true
    static var isReadINE = false
    static var isReadPassport = false
    static var isReadProofOfAddress = false
    static var isProofLifeSelfie = false
    static var timeToTakeSelfie = 3
    
    static var isLoadVerificationModule = false
    static var isLoadSignModule = false
    
    static var isSaveProcess = false
    static var isLoadCurrentProcess = false
    
    static var startCheckpoint = -1
    
    static var pdfData : Data?
    static var xmlData: Data?
    
    static var mainColor : UIColor?
    static var secondaryColor : UIColor?
    static var backGroundColor : UIColor?
    static var imgCompany : UIImage?
    
    static var isAddMailScreen = false
    
    static var openEyesProbability : CGFloat = 0.85
    static var closeEyesProbability : CGFloat = 0.50
    static var smileProbability : CGFloat = 0.37
    
    static var isShowPrivacyView :  Bool = true
    
    
     static var imgSelfie : UIImage?
    
}
