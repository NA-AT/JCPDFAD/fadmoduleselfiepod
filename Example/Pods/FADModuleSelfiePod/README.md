# FADModuleSelfiePod

[![CI Status](https://img.shields.io/travis/jcjobs/FADModuleSelfiePod.svg?style=flat)](https://travis-ci.org/jcjobs/FADModuleSelfiePod)
[![Version](https://img.shields.io/cocoapods/v/FADModuleSelfiePod.svg?style=flat)](https://cocoapods.org/pods/FADModuleSelfiePod)
[![License](https://img.shields.io/cocoapods/l/FADModuleSelfiePod.svg?style=flat)](https://cocoapods.org/pods/FADModuleSelfiePod)
[![Platform](https://img.shields.io/cocoapods/p/FADModuleSelfiePod.svg?style=flat)](https://cocoapods.org/pods/FADModuleSelfiePod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FADModuleSelfiePod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FADModuleSelfiePod'
```

## Author

jcjobs, jcperez@na-at.com.mx

## License

FADModuleSelfiePod is available under the MIT license. See the LICENSE file for more info.
