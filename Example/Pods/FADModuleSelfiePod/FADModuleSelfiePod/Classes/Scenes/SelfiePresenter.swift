//
//  SelfiePresenter.swift
//  FADModulePhoto
//
//  Created by Juan Carlos Pérez on 1/25/19.
//  Copyright (c) 2019 Juan Carlos Pérez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol SelfiePresentationLogic
{
    func presentInitialConfigResult(response: Selfie.Camera.Session)
    func presentCameraSession(response: Selfie.Camera.Session)
    func presentTakeSelfieAgain()
}

class SelfiePresenter: SelfiePresentationLogic
{
    
    
  weak var viewController: SelfieDisplayLogic?
  
  // MARK: Do something
  
    func presentInitialConfigResult(response: Selfie.Camera.Session){
        
        var viewModel = Selfie.Config.ViewModel()
        viewModel.companyDynamicImage = Utils.getCompanyDynamicImage()
        viewModel.isProofLifeSelfie = Constants.isProofLifeSelfie
        
        viewModel.camSession = response.camSession
        viewModel.frontCameraDevice = response.frontCameraDevice
        
        
        viewController?.displayInitialConfigResult(viewModel: viewModel)
        viewController?.startViewSession()
        viewController?.startSessionRecognition()
        viewController?.generateRandomProofOfLife()
        viewController?.endInitialConfigResult()
        
        if let selfie = Constants.imgSelfie{
            viewController?.displayPhotoPreview(selfie: selfie)
        }
    }
    
    
    
    
    func presentCameraSession(response: Selfie.Camera.Session) {
        viewController?.displayCameraSession(response: response)
    }
    
    
    
    func presentTakeSelfieAgain() {
        viewController?.takeSelfieAgain()
        viewController?.generateRandomProofOfLife()
    }
    
    
}
