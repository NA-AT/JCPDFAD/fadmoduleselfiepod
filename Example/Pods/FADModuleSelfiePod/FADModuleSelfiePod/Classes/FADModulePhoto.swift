//
//  FADModulePhoto.swift
//  FADModulePhoto
//
//  Created by Juan Carlos Pérez on 1/23/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit
import FirebaseCore

public class FADModulePhoto: NSObject {
    
    public static var initProcessOnSuccess : ((_ imgSelfie:UIImage)->Void)?
    public static var initProccessOnCancel : (()->Void)?
    public static var initProccessOnError : (()->Void)?
    
    
    public static var storyBoardNav : UINavigationController?

    
    public static func initProccess(window : UIWindow, constants:FADSelfieConstants, onSuccess:@escaping (_ imgSelfie:UIImage)-> Void, onCancel:@escaping ()->Void, onError:@escaping ()-> Void) {
    
        FADModulePhoto.registerFireBase()
        
        do {
            try Utils.fontsURLs().forEach({ try UIFont.register(from: $0) })
        } catch {
            print(error)
        }
        
        
        Constants.isSaveProcess = constants.isSaveProcess
        Constants.isProofLifeSelfie = constants.isProofLifeSelfie
        Constants.timeToTakeSelfie = constants.timeToTakeSelfie
        Constants.openEyesProbability = constants.openEyesProbability
        Constants.closeEyesProbability = constants.closeEyesProbability
        Constants.smileProbability = constants.smileProbability
        Constants.imgSelfie = constants.imgSelfie
        
        
        initProcessOnSuccess = onSuccess
        initProccessOnCancel = onCancel
        initProccessOnError = onError
        
        
        
        let storyboard = UIStoryboard(name: "Selfie", bundle: Utils.getSDKBundle())
        let vc = storyboard.instantiateViewController(withIdentifier: "fadSelfieViewControllerId") as! SelfieViewController
        //UIApplication.shared.keyWindow?.rootViewController?.navigationController?.pushViewController(vc, animated: true)
        //window.rootViewController?.navigationController?.pushViewController(vc, animated: true)
        
        
        //Opcion1:
        if let storyBoardNavigation = window.rootViewController as? UINavigationController{
            self.storyBoardNav = storyBoardNavigation
            
            //storyBoardNavigation.pushViewController(vc, animated: true)
            var arrayViewControllers = storyBoardNavigation.viewControllers
            arrayViewControllers.append(vc)
            
            //storyBoardNavigation.setViewControllers([vc], animated: true)
            storyBoardNavigation.setViewControllers(arrayViewControllers, animated: true)
            
            window.rootViewController? = storyBoardNavigation
            window.makeKeyAndVisible()
        }
 
        
        
        /*
        //Opcion2:
        if let storyBoardNavigation = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController{
            storyBoardNavigation.pushViewController(vc, animated: true)
            if let windowRoot = UIApplication.shared.windows.first {
                windowRoot.rootViewController? = storyBoardNavigation
                windowRoot.makeKeyAndVisible()
            }
        }
        */
        
        /*
        //Opcion3:
        if let storyBoardNavigation = UIStoryboard(name: "Selfie", bundle: Utils.getSDKBundle() ).instantiateViewController(withIdentifier: "fadSelfieViewControllerId") as? UINavigationController{
            print("Load Storyboard")
            storyBoardNav = storyBoardNavigation
            window.rootViewController? = storyBoardNav!
            window.makeKeyAndVisible()
        }
        */
        
        
        
        
    }
    
    
    public static func registerFireBase () {
       
        print("Configure SDK")
        
        if FirebaseApp.app() == nil {
            print("SDK APP ❌")
            
            let filePath = Utils.getSDKBundle().path(forResource: "GoogleService-Info", ofType: "plist")!
            let options = FirebaseOptions(contentsOfFile: filePath)
            FirebaseApp.configure(options: options!)
            
            print("SDK APP ✅")
            
        }else{
            print("SDK APP ✅")
        }
        
    }
    
}
