//
//  Signature.swift
//  FADModulePhoto
//
//  Created by Juan Carlos Pérez on 1/23/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//


import UIKit

class Signature: NSObject {
    var averageSpeed : String = ""
    var unitAverageSpeed : String = ""
    var spaces : String = ""
    var attackPoints : String = ""
    var signLenght : String = ""
    var signDuration : String = ""
    var unitSignLenght : String = ""
    var unitSignDuration : String = ""
    var signaturePoints : [SignaturePoint] = []
    
    
    func calculateSpeed () {
        /*
         double total_pixels = getTotalLengthinPixels();
         double totalSecs = getTotalDurationinSeconds();
         double speed = total_pixels/totalSecs;
         averageSpeed = speed+"";
         signLenght = total_pixels+"";
         signDuration =  totalSecs + "" ;
         unitSignLength = "px";
         unitSignDuration  = "s";*/
        
        let totalPixels = getTotalLenghtInPixels()
        let totalSecs = getTotalDurationInSeconds()
        let speed = totalPixels / totalSecs
        
        self.averageSpeed = "\(speed)" //speed
        self.unitAverageSpeed = "px/s"
        
        let intAttack = Int(attackPoints)
        self.spaces = "\(intAttack! - 1)"
        
        self.signLenght = "\(totalPixels)"
        self.signDuration = "\(totalSecs)"
        
        self.unitSignLenght = "px"
        self.unitSignDuration = "s"
        
    }
    
    
    func distanceBetweenPoints(p1: SignaturePoint, p2: SignaturePoint) -> Double{
        let xp1 = Double(p1.x)
        let yp1 = Double(p1.y)
        
        let xp2 = Double(p2.x)
        let yp2 = Double(p2.y)
        
        let cat1 =  pow(xp1! - xp2!, 2)
        let cat2 =  pow(yp1! - yp2!, 2)
        
        let distance = sqrt(cat1+cat2)
        return distance
        
    }
    
    func getTotalLenghtInPixels() -> Double{
        var totalLenght : Double = 0
        let signaturePointCount = signaturePoints.count - 1
        for i in 0..<signaturePointCount {
            let p1 = signaturePoints[i]
            let p2 = signaturePoints[i + 1]
            totalLenght = totalLenght + distanceBetweenPoints(p1: p1, p2: p2)
        }
        return totalLenght
    }
    
    func getTotalDurationInSeconds () -> Double {
        let initialTimeStamp = signaturePoints[0].timeStamp
        let finalTimeStamp = signaturePoints[signaturePoints.count - 1].timeStamp
        let totalTime = (finalTimeStamp - initialTimeStamp)/1000
        return totalTime
    }
    
}
