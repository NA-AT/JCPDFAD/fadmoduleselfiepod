//
//  ViewController.swift
//  FADModuleSelfiePod
//
//  Created by jcjobs on 01/24/2019.
//  Copyright (c) 2019 jcjobs. All rights reserved.
//

import UIKit
import FADModuleSelfiePod

class ViewController: UIViewController {
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var contentView : UIView!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        let storyboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "emptyViewController") as! EmptyViewController
        
        let navController = UINavigationController(rootViewController:vc)
        navController.isNavigationBarHidden = true
        self.addChild(navController)
        navController.view.frame = contentView.bounds
        contentView.addSubview(navController.view)
    
    }
    
    
    func callSelfieSDK(){
        /*
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let window = appDelegate.window
        //FADModulePhoto.initProccess(window: appDelegate.window!)
        */
        
        let constants : FADSelfieConstants  = FADSelfieConstants()
        constants.isSaveProcess = true//*
        constants.isProofLifeSelfie = true
        constants.timeToTakeSelfie = 3
        constants.openEyesProbability = 0.85
        constants.closeEyesProbability = 0.50
        constants.smileProbability = 0.37
        
        //constants.imgSelfie = UIImage(named: "logoFADB")
        let navigation = self.parent as? UINavigationController
        
        FADModulePhoto.initProccess(navigation: navigation, constants: constants, onSuccess: {imgSelfie in
            print("onSucces")
            self.photoView.image = imgSelfie
             self.navigationController?.popToRootViewController(animated: true)
        },onCancel: {
            print("onCancel")
            self.navigationController?.popToRootViewController(animated: true)
        },onError: {
            print("onError")
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
    
    
    @IBAction func btnTakeSelfie(_ sender: Any) {
        self.callSelfieSDK()
    }
    
}

